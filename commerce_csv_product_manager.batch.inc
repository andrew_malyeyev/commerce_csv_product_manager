<?php
/**
 * Process every item in batch Export
 */
function commerce_csv_product_manager_process_product_export($products, &$context) {

  $limit = 1;

  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($products);
  }

  if(empty($context['sandbox']['items'])) {
    $context['sandbox']['items'] = $products;
  }

  $counter = 0;
  if(!empty($context['sandbox']['items'])) {

    if ($context['sandbox']['progress'] != 0) {
      array_splice($context['sandbox']['items'], 0, $limit);
    }
    else {
      $context['results']['header'] = CommerceCSVProductManage::GetHeaderForCSVFile();
    }

    foreach ($context['sandbox']['items'] as $sku) {
      if ($counter != $limit) {

        $product = commerce_product_load_by_sku($sku);
        $context['results']['data'][] = CommerceCSVProductManagerExport::GetDataForExportFile($product);

        $counter++;
        $context['sandbox']['progress']++;

        $context['message'] = t('Now Exporting products %progress of %count<br />SKU: %sku<br />Tile: %title', array(
            '%progress' => $context['sandbox']['progress'],
            '%count' => $context['sandbox']['max'],
            '%title' => $product->title,
            '%sku' => $product->sku,
          )
        );
        $context['results']['processed'] = $context['sandbox']['progress'];
      }
    }
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function commerce_csv_product_manager_process_export_finished($success, $results, $operations) {
  if ($success) {
    $file_uri = CommerceCSVProductManagerExport::GenerateFileExport($results['header'], $results['data']);

    if ($file_uri) {

      $message_info = format_plural($results['processed'], 'One Product was exported.', '@count Products was exported');
      $message_file = t('Download the !file of products exports', array('!file' => l('file', file_create_url($file_uri))));

      drupal_set_message(t($message_info), 'status');
      drupal_set_message($message_file, 'status');
    }
    else {
      $message_error = t('The export file not created. Check permission to default folder');
      drupal_set_message($message_error, 'error');
    }
  }
  else {
    $message = t('Product export finished with an error!');
    drupal_set_message($message, 'error');
  }
}