<?php
/**
 * file
 */

/**
 * Implements hook_form().
 */
function commerce_csv_product_manager_settings_form() {

  $product_types = commerce_product_types();

  if (empty($product_types)) {
    drupal_set_message(t('You do not have any product types!'), 'warning');
    return '';
  }
  else {
    $form['#tree'] = TRUE;
    $form['#theme'] = 'commerce_csv_product_manager_theme';
    $form['commerce_csv_product_manager_settings'] = array(
      '#type' => 'item',
      '#description' => t('More information about CSV format see !here', array('!here' => l('here', COMMERCE_CSV_PRODUCT_MANAGER_CSV_WIKI))),
    );
    $form['commerce_csv_product_manager_settings_delimiter'] = array(
      '#type' => 'textfield',
      '#title' => t('Delimiter'),
      '#default_value' => CommerceCSVProductManager::GetDelimiter(),
      '#size' => 1,
      '#maxlength' => 1,
      '#required' => TRUE,
      '#description' => t('This is delimiter for a csv file'),
    );
    $form['commerce_csv_product_manager_settings_separate'] = array(
      '#type' => 'textfield',
      '#title' => t('Separate'),
      '#default_value' => CommerceCSVProductManager::GetSeparate(),
      '#size' => 1,
      '#maxlength' => 1,
      '#required' => TRUE,
      '#description' => t('This is separate for a csv file'),
    );
    $form['commerce_csv_product_manager_table'] = CommerceCSVProductManagerForm::FormSettingData($product_types);
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save settings'),
    );

    return $form;
  }
}

/**
 * Implements hook_form_validate().
 */
function commerce_csv_product_manager_settings_form_validate($form, $form_state) {
  foreach ($form_state['values']['commerce_csv_product_manager_table'] as $machine_name => $value) {
    if (empty($value['commerce_csv_product_manager_label'])) {
      form_set_error($machine_name, t('Label for "@field" field is required.', array('@field' => $machine_name)));
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function commerce_csv_product_manager_settings_form_submit($form, $form_state) {
  $status = CommerceCSVProductManagerForm::FormSettingDataSubmitTableAction($form_state['values']['commerce_csv_product_manager_table']);

  CommerceCSVProductManagerForm::FormSettingDataSubmitTableDelimiterAction($form_state['values']['commerce_csv_product_manager_settings_delimiter']);
  CommerceCSVProductManagerForm::FormSettingDataSubmitTableSeparateAction($form_state['values']['commerce_csv_product_manager_settings_separate']);

  if ($status) {
    drupal_set_message(t('Settings have been saved'));
  }
}

/**
 * Implements hook_form().
 */
function commerce_csv_product_manager_export_form() {

  $count_products = CommerceCSVProductManager::CountProducts();

  if (empty($count_products)) {
    drupal_set_message(t('Neither product was created! Create a !product', array('!product' => l('product', COMMERCE_PIEUD_URI_CREATE_PRODUCT))), 'warning');
  }
  else {
    $form = array();
    $form['markup'] = array(
      '#type' => 'item',
      '#description' => t('Push button for start export'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Product Export'),
    );
    return $form;
  }
}

/**
 * Implements hook_form_submit().
 */
function commerce_csv_product_manager_export_form_submit($form, &$form_state) {

  $data = CommerceCSVProductManager::GetProductsSKU();

  $batch = array(
    'title' => t('Product export processing'),
    'operations' => array(
      array('commerce_csv_product_manager_process_product_export', array($data)),
    ),
    'progress_message' => '',
    'finished' => 'commerce_csv_product_manager_process_export_finished',
    'file' => drupal_get_path('module', 'commerce_csv_product_manager') . '/commerce_csv_product_manager.batch.inc',
  );
  batch_set($batch);
  batch_process();
}

/**
 * Implements hook_form().
 */
function commerce_csv_product_manager_import_form() {
  $form['file'] = array(
    '#type' => 'managed_file',
    '#title' => t('File for import products'),
    '#description' => t('This file must be like *csv'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('csv'),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import Products'),
  );

  return $form;
}