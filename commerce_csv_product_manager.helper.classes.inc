<?php
/**
 * File
 */

/**
 * Helper classes
 * Commerce CSV Product Manage
 */
class CommerceCSVProductManager {
  public static function GetDelimiter() {
    return variable_get('commerce_csv_product_manager_delimiter', COMMERCE_CSV_PRODUCT_MANAGER_DEFAULT_DELIMITER);
  }

  public static function GetSeparate() {
    return variable_get('commerce_csv_product_manager_separate', COMMERCE_CSV_PRODUCT_MANAGER_DEFAULT_SEPARATE);
  }

  public static function CountProducts() {
    return db_query('SELECT COUNT(product_id) FROM {commerce_product}')->fetchField();
  }

  public static function GetMachineNameFromLabel($label = NULL) {
    return db_query('SELECT machine_name FROM {commerce_csv_product_manager_fields} WHERE label = :label', array(':label' => $label))->fetchField();
  }

  public static function GetFieldLabel($field_name = NULL, $all_fields = array()) {
    if (!empty($field_name) && is_array($all_fields) && !empty($all_fields)) {
      $label = self::GetField($field_name, 'label');

      if (!$label) {
        if (isset($all_fields[$field_name])) {
          return $all_fields[$field_name];
        }
      }

      return $label;
    }

    return FALSE;
  }

  public static function GetField($field_name = NULL, $column = NULL) {
    $value = db_select('commerce_csv_product_manager_fields', 'm')
      ->fields('m', array($column))
      ->condition('m.machine_name', $field_name)
      ->execute()
      ->fetchField();

    return $value;
  }

  public static function GetAllFields($product_types = NULL) {
    if (!empty($product_types)) {
      global $commerce_csv_product_manager_default_fields;

      $fields = $commerce_csv_product_manager_default_fields;

      foreach ($product_types as $type_value) {
        $custom_fields = field_info_instances(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $type_value['type']);
        foreach ($custom_fields as $field_machine_name => $field_value) {
          $fields[$field_machine_name] = $field_value['label'];
        }
      }

      return $fields;
    }

    return array();
  }

  public static function GetFileName() {
    return variable_get('site_name', '') . '.products_' . date('d_m_Y') . '.csv';
  }

  public static function GetLabelFromMachineName($machine_name = NULL) {
    return db_query('SELECT label FROM {commerce_csv_product_manager_fields} WHERE machine_name = :machine_name', array(':machine_name' => $machine_name))->fetchField();
  }

  public static function GetMaxWeightFields($all_fields) {
    $count_by_fields = count($all_fields);
    $count_by_fields_db = self::GetMinWeightFields();

    if ($count_by_fields_db) {
      $count_by_fields_db_p = abs($count_by_fields_db['max']);
      $count_by_fields_db_m = abs($count_by_fields_db['min']);

      return max(array($count_by_fields_db_p, $count_by_fields_db_m, $count_by_fields));
    }

    return $count_by_fields;
  }

  public static function GetMinWeightFields() {
    $max_count = db_query('SELECT MAX(weight) FROM {commerce_csv_product_manager_fields}')->fetchField();
    $min_count = db_query('SELECT MIN(weight) FROM {commerce_csv_product_manager_fields}')->fetchField();

    if (empty($max_count) || empty($min_count)) {
      return FALSE;
    }

    $count['max'] = $max_count;
    $count['min'] = $min_count;

    return $count;
  }

  public static function GetWeightFieldsByField($field_name = '', &$weight) {

    if (!empty($field_name)) {
      $query_weight = self::GetField($field_name, 'weight');
      if (!$query_weight) {
        ++$weight;
        return $weight;
      }
      return $query_weight;
    }
    return FALSE;
  }

  public static function GetFieldStatus($field_name = '') {
    if (!empty($field_name)) {
      $label = self::GetField($field_name, 'status');
      return $label;
    }
    return FALSE;
  }

  public static function GetProductsSKU() {
    $result = db_select('commerce_product')
      ->fields('commerce_product', array(COMMERCE_CSV_PRODUCT_MANAGER_SKU))
      ->orderBy('product_id')
      ->execute()
      ->fetchAll();

    $output = array();

    if (!empty($result)) {
      foreach ($result as $product) {
        $output[] = $product->sku;
      }
    }

    return $output;
  }

  static  public function GetEnableFields() {
    $result = db_select('commerce_csv_product_manager_fields', 'm')
      ->fields('m', array('machine_name'))
      ->condition('m.status', 1)
      ->orderBy('weight')
      ->execute()
      ->fetchAll();

    $output = array();

    if (!empty($result)) {
      foreach ($result as $product) {
        $output[] = $product->machine_name;
      }
    }

    return $output;
  }

  public static function GetHeaderForCSVFile() {
    $result = db_select('commerce_csv_product_manager_fields', 'm')
      ->fields('m', array('label'))
      ->condition('m.status', 1)
      ->orderBy('weight')
      ->execute()
      ->fetchAll();

    $output = array();

    if (!empty($result)) {
      foreach ($result as $product) {
        $output[] = $product->label;
      }
    }

    return $output;
  }

  static  public function GetEnableFieldsLabel() {
    $result = db_select('commerce_csv_product_manager_fields', 'm')
      ->fields('m', array('machine_name', 'label'))
      ->condition('m.status', 1)
      ->orderBy('weight')
      ->execute()
      ->fetchAll();

    $output = array();

    if (!empty($result)) {
      foreach ($result as $product) {
        $output[$product->machine_name] = $product->label;
      }
    }

    return $output;
  }


  /*public static function GetFileTempPath($path) {
    //file_directory_temp()
    //$uri = file_default_scheme() . '://' . $path;
    $uri = 'temporary' . '://' . $path;
    return file_stream_wrapper_uri_normalize($uri);
  }*/

}

/**
 * Helper classes
 * Commerce CSV Product Manage Info
 */
class CommerceCSVProductManagerInfo extends CommerceCSVProductManager {
  static public function PageInfo() {
    return 'Info';
  }
}

/**
 * Helper Form classes
 * Commerce CSV Product Manage Form
 */
class CommerceCSVProductManagerForm extends CommerceCSVProductManager {

  public static function FormSettingData($product_types = '') {
    $all_fields = parent::GetAllFields($product_types);

    if (!empty($all_fields)) {
      $data = array();
      $weight = -1;
      $delta = parent::GetMaxWeightFields($all_fields);

      foreach ($all_fields as $field_name => $field_lable) {

        $data[$field_name] = array(

          'commerce_csv_product_manager_label' => array(
            '#type' => 'textfield',
            '#default_value' => parent::GetFieldLabel($field_name, $all_fields),
            '#required' => TRUE,
            '#weight' => 1,
          ),
          'commerce_csv_product_manager_name' => array(
            '#markup' => $field_name,
            '#weight' => 2,
          ),
          'commerce_csv_product_manager_weight' => array(
            '#type' => 'weight',
            '#default_value' => parent::GetWeightFieldsByField($field_name, $weight),
            '#weight' => 3,
            '#delta' => $delta,
          ),
        );

        if ($field_name == 'sku' || $field_name == 'product_type') {
          $data[$field_name]['commerce_csv_product_manager_status'] = array(
            '#type' => 'hidden',
            '#value' => 1,
            '#prefix' => '<div>' . t('This field is required'),
            '#suffix' => '</div>',
            '#weight' => 0,
          );
        }
        else {
          $data[$field_name]['commerce_csv_product_manager_status'] = array(
            '#type' => 'checkbox',
            '#default_value' => parent::GetFieldStatus($field_name),
            '#weight' => 0,
          );
        }
      }
    }

    return $data;
  }

  public static function FormSettingDataSubmitTableAction($data = array()) {

    $count_error = 0;

    foreach ($data as $field_name => $field_value) {

      $field_id = db_query('SELECT field_id FROM {commerce_csv_product_manager_fields} WHERE machine_name = :machine_name', array(':machine_name' => $field_name))->fetchField();

      if ($field_id) {
        db_update('commerce_csv_product_manager_fields')
          ->fields(array(
            'status' => $field_value['commerce_csv_product_manager_status'],
            'label' => $field_value['commerce_csv_product_manager_label'],
            'machine_name' => $field_name,
            'weight' => $field_value['commerce_csv_product_manager_weight'],
          ))
          ->condition('field_id', $field_id)
          ->execute();
      }
      else {
        db_insert('commerce_csv_product_manager_fields')
          ->fields(array(
            'status' => $field_value['commerce_csv_product_manager_status'],
            'label' => $field_value['commerce_csv_product_manager_label'],
            'machine_name' => $field_name,
            'weight' => $field_value['commerce_csv_product_manager_weight'],
          ))
          ->execute();
      }
    }

    if ($count_error > 0) {
      return FALSE;
    }

    return TRUE;
  }

  public static function FormSettingDataSubmitTableDelimiterAction($delimiter = COMMERCE_CSV_PRODUCT_MANAGER_DEFAULT_DELIMITER) {
    variable_set('commerce_csv_product_manager', $delimiter);
  }

  public static function FormSettingDataSubmitTableSeparateAction($separate = COMMERCE_CSV_PRODUCT_MANAGER_DEFAULT_SEPARATE) {
    variable_set('commerce_csv_product_manager', $separate);
  }
}

/**
 * Helper classes
 * Commerce CSV Product Manage Export
 */
class CommerceCSVProductManagerExport extends CommerceCSVProductManager {

  public static function GenerateFileExport($header, $data) {
    if (is_array($header) && is_array($data)) {

      $separate  = parent::GetSeparate();
      $delimiter = parent::GetDelimiter();
      $filename  = parent::GetFileName();
      $path_file = file_build_uri($filename);
      //$path_file = file_directory_temp() . '/' . $filename;
      //$path_file = parent::GetFileTempPath($filename);
      $save_directory = drupal_realpath($path_file);

      // Open file stream
      $out_stream = fopen($path_file, 'w');

      // Add columns
      fputcsv($out_stream, $header, $delimiter, $separate);

      foreach ($data as $value) {
        fputcsv($out_stream, $value, $delimiter, $separate);
      }

      fclose($out_stream);

      if (file_exists(drupal_realpath($path_file)) && is_readable(drupal_realpath($path_file))) {
        return  $path_file;
      }
    }

    return FALSE;
  }


  public static function GetDataForExportFile($product = NULL) {
    if (is_object($product)) {

      $enable_fields = CommerceCSVProductManager::GetEnableFields();
      $product_type  = $product->type ;
      $output        = array();

      foreach ($enable_fields as $field) {
        if ($field == COMMERCE_CSV_PRODUCT_MANAGER_PRODUCT_TYPE) {
          $output[] = $product_type;
        }
        else {
          $export = new CommerceCSVProductManagerExport();
          $output[] = $export->GetFieldValue($product, $field);
        }
      }

      return $output;
    }

    return array();
  }

  public function GetFieldValue($product = NULL, $field = NULL) {

    if (is_object($product) && !empty($field) && isset($product->{$field})) {

      switch ($field) {
        case COMMERCE_CSV_PRODUCT_MANAGER_SKU:
          return $product->sku;
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_STATUS:
          return $product->status;
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_TITLE_TYPE:
          return $product->title;
          break;
      }

      $field_info = field_info_field($field);

      switch ($field_info['type']) {

        case COMMERCE_CSV_PRODUCT_MANAGER_TERM_TYPE:
          return $this->GetFieldValueTaxonomyTermReference($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_PRODUCT_REF_TYPE:
          return $this->GetFieldValueCommerceProductReference($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_NUMB_FL_TYPE:
          return $this->GetFieldValueNumberFloat($product, $field, $field_info);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_TEXT_TYPE:
          return $this->GetFieldValueText($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_TYPE_PRICE:
          return $this->GetFieldValueCommercePrice($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_IMG_TYPE:
          return $this->GetFieldValueFile($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_FILE_TYPE:
          return $this->GetFieldValueFile($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_TEXT_LONG_TYPE:
          return $this->GetFieldValueText($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_TEXT_SUMMARY_TYPE:
          return $this->GetFieldValueText($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_NUMBER_DECIMAL_TYPE:
          return $this->GetFieldValueText($product, $field);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_LIST_BOOL_TYPE:
          return $this->GetFieldValueList($product, $field, $field_info);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_LIST_FLOAT_TYPE:
          return $this->GetFieldValueList($product, $field, $field_info);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_LIST_TEXT_TYPE:
          return $this->GetFieldValueList($product, $field, $field_info);
          break;

        case COMMERCE_CSV_PRODUCT_MANAGER_LIST_INT_TYPE:
          return $this->GetFieldValueList($product, $field, $field_info);
          break;

        default :
          return;
          break;
      }
    }

    return;
  }

  public function GetFieldValueTaxonomyTermReference($product = NULL, $field = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $term = taxonomy_term_load($value['tid']);
        if (isset($term->name)) {
          $output[]=  $term->name;
        }
      }
    }

    return implode($delimiter, $output);
  }

  public function GetFieldValueCommerceProductReference($product = NULL, $field = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $product = commerce_product_load($value['product_id']);
        if (isset($product->sku)) {
          $output[] = $product->sku;
        }
      }
    }

    return implode($delimiter, $output);
  }

  public function GetFieldValueNumberFloat($product = NULL, $field = NULL, $field_info = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $decimal_separator = $field_info['settings']['decimal_separator'];
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $output[] = str_replace(COMMERCE_CSV_PRODUCT_MANAGER_DEFAULT_DECIMAL_SER, $decimal_separator, $value['value']);
      }
    }

    return implode($delimiter, $output);
  }

  public function GetFieldValueText($product = NULL, $field = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $output[] = $value['value'];
      }
    }

    return implode($delimiter, $output);
  }

  public function GetFieldValueCommercePrice($product = NULL, $field = NULL) {
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      if (isset($field_items[0]['amount']) && isset($field_items[0]['currency_code'])) {
        return ($field_items[0]['amount'] / COMMERCE_CSV_PRODUCT_MANAGER_PRICE_DIVISOR) . $field_items[0]['currency_code'];
      }
    }

    return;
  }

  public function GetFieldValueFile($product = NULL, $field = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $output[] = $value['uri'];
      }
    }

    return implode($delimiter, $output);
  }

  public function GetFieldValueList($product = NULL, $field = NULL, $field_info = NULL) {
    $output = array();
    $delimiter = parent::GetDelimiter();
    $field_items = field_get_items(COMMERCE_CSV_PRODUCT_MANAGER_ENTITY_TYPE, $product, $field);

    if ($field_items) {
      foreach ($field_items as $value) {
        $output[] = isset($field_info['settings']['allowed_values'][$value['value']]) ? $field_info['settings']['allowed_values'][$value['value']] : NULL;
      }
    }

    return implode($delimiter, $output);
  }
}