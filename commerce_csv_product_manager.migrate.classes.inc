<?php
/**
 * File
 */
class CommerceCSVProductManagerMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Import Products');

    $columns_options = CommerceCSVProductManager::GetEnableFieldsLabel();

    $columns = array();

    foreach ($columns_options as $machine_name => $label) {
      $columns[] = array($machine_name, t($label));
    }

    $options = array(
      'header_rows' => 1,
      'delimiter' => CommerceCSVProductManager::GetDelimiter(),
      'enclosure' => CommerceCSVProductManager::GetSeparate(),
    );

    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'test_migrate_csv') . '/test.csv', $columns, $options);
    $this->destination = new MigrateDestinationNode('page');

    $source_key_schema = array(
      'id_csv' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      )
    );

    $this->map = new MigrateSQLMap($this->machineName, $source_key_schema, MigrateDestinationNode::getKeySchema());

    $this->addFieldMapping('title', 'title_csv');
    $this->addFieldMapping('body', 'body_csv');
  }
}